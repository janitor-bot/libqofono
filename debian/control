Source: libqofono
Section: libs
Priority: optional
Maintainer: Debian UBports Team <team+ubports@tracker.debian.org>
Uploaders:
 Mike Gabriel <sunweaver@debian.org>,
Build-Depends: debhelper-compat (= 13),
               pkg-config,
               qtbase5-dev,
               qtdeclarative5-dev,
               qml-module-qtquick2,
               libqt5xmlpatterns5-dev,
               pkg-kde-tools,
Standards-Version: 4.6.1
Rules-Requires-Root: no
Homepage: https://github.com/sailfishos/libqofono
Vcs-Git: https://salsa.debian.org/ubports-team/libqofono.git
Vcs-Browser: https://salsa.debian.org/ubports-team/libqofono/

Package: libqofono-qt5-0
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Multi-Arch: same
Depends: ${shlibs:Depends},
         ${misc:Depends},
Suggests: ofono (>= 1.16),
Description: Qt library for Ofono
 Shared library for accessing the ofono daemon, and a declarative plugin
 for it. This allows accessing ofono in qtquick and friends.
 .
 This package contains the Qt5 build of the libqofono library.

Package: libqofono-dev
Section: libdevel
Architecture: any
Depends: ${misc:Depends},
         libqofono-qt5-0 (= ${binary:Version}),
Description: Qt library for Ofono (development files)
 Shared library for accessing the ofono daemon, and a declarative plugin
 for it. This allows accessing ofono in qtquick and friends.
 .
 This package contains the header files of the libqofono shared library.

Package: libqofono-tests
Section: libdevel
Architecture: any
Multi-Arch: foreign
Depends: ${shlibs:Depends},
         ${misc:Depends},
         libqofono-qt5-0 (= ${binary:Version}),
Description: Qt library for Ofono (unit tests)
 Shared library for accessing the ofono daemon, and a declarative plugin
 for it. This allows accessing ofono in qtquick and friends.
 .
 This package contains the unit tests of the shared library. These
 require ofono to be running. Before they can be run at build-time, more
 upstream work is required. As an alternative, the executable unit tests
 are provided for run-time.

Package: libqofono-examples
Section: libdevel
Architecture: any
Multi-Arch: foreign
Depends: ${shlibs:Depends},
         ${misc:Depends},
         libqofono-qt5-0 (= ${binary:Version}),
Description: QtQuick/QML example application for libqofono-qt5
 Shared library for accessing the ofono daemon, and a declarative plugin
 for it. This allows accessing ofono in qtquick and friends.
 .
 This package contains the ofonotest example application written in
 QtQuick/QML.

Package: qml-module-ofono
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Multi-Arch: same
Depends: ${shlibs:Depends},
         ${misc:Depends},
Breaks: ubuntu-system-settings (<< 0.3+15.04.20150114),
        qtdeclarative5-ofono0.2,
Replaces: qtdeclarative5-ofono0.2
Description: QML bindings for libqofono
 Shared library for accessing the ofono daemon, and a declarative plugin
 for it. This allows accessing ofono in qtquick and friends.
 .
 This package contains the QML bindings for the libqofono shared library.
